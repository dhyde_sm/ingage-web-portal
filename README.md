Ingage Web Portal
=================

The composed stack for Ingage Web Portal.

### Environment Configuration

Use these values in the `.env` file to enable a specific environment

| Environment Name |
|------------------|
| development      |
| staging          |
| production       |

### Development

1\. Clone this repository

```shell
git clone https://bitbucket.org/scrollmotiongit/ingage-web-portal.git

cd ingage-web-portal
```

2\. Start server on development mode
```shell

# change to server directory
cd server/

# start server using docker-compose
docker-compose up

# visit http:localhost:8080
```

3\. Start client on development mode
```shell

# change to client directory
cd client/

# start client using docker-compose
docker-compose up

# visit http:localhost:8080
```

### Deployment

1\. Clone this repository

```shell
git clone https://bitbucket.org/scrollmotiongit/ingage-web-portal.git

cd ingage-web-portal
```

3\. Configue environment and port in the `.env` file

2\. Start the application

```shell
docker-compose up
```
