// dependencies
import Vue    from 'vue';
import store  from './store';
import router from './router';

// api host name
import { HOST_NAME, CLIENT_ID } from './api/host';

const LOGIN_URL            = HOST_NAME + '/oauth2/token/';
const RECOVER_PASSWORD_URL = HOST_NAME + '/password/forgot/';
const RESET_PASSWORD_URL   = HOST_NAME + '/password/reset/';
const REGISTER_URL         = HOST_NAME + '/sign_up/';
const COUPON_URL           = HOST_NAME + '/smpayments/coupons/';

const LOGIN_HEADERS = {
    emulateJSON: true
};

const REGISTER_HEADERS = {
    headers: {
        'X-CLIENT-ID': CLIENT_ID
    },
    emulateJSON: true
};

export default {
    install (Vue, options) {
        Vue.http.interceptors.push((request, next) => {
            const token         = store.state.auth.accessToken;
            const hasAuthHeader = request.headers.has('Authorization');

            if (token && !hasAuthHeader) {
                this.setAuthHeader(request)
            }

            next((response) => {
                if (this._isInvalidToken(response) && request.url !== LOGIN_URL) {
                    return this._refreshToken(request);
                }
            });
        });

        Vue.prototype.$auth = Vue.auth = this;
    },

    login (credentials) {
        const params = {
            client_id:  CLIENT_ID,
            grant_type: 'password',
            username:   credentials.username,
            password:   credentials.password,
            clear_all:  true
        };

        return Vue.http.post(LOGIN_URL, params, LOGIN_HEADERS)
            .then(response => {
                this._storeToken(response);

                return response;
            })
    },

    socialAuth (user, token, backend, merge=false, password) {
        const params = {
            client_id: CLIENT_ID,
            backend,
            token,
            merge
        };

        if (password) {
            params.password = password;
        }

        return Vue.http.post(HOST_NAME + '/sso/social_auth/', params)
            .then(response => {
                this._storeToken(response);

                return response;
            })
            .then(response => {
                this.registerMixpanelUser({
                    id:         response.body.user.id,
                    username:   user.email,
                    first_name: user.firstName,
                    last_name:  user.lastName
                });

                return response;
            });
    },

    logout (name='login') {
        store.commit('CLEAR_ALL_DATA');

        // have mixpanel unset current user
        mixpanel.reset();
        mixpanel.ingage_instants.reset();
        mixpanel.ingage_stories.reset();

        router.push({ name });
    },

    requestNewPassword (credentials) {
        return Vue.http.post(RECOVER_PASSWORD_URL, credentials, REGISTER_HEADERS);
    },

    resetPassword (credentials) {
        const params = {
            email:      credentials.email,
            passkey:    credentials.passkey,
            password:   credentials.password
        };

        return Vue.http.post(RESET_PASSWORD_URL, params, REGISTER_HEADERS)
            .then(response => response);
    },

    register (credentials, affiliateId) {
        const registerUrl = affiliateId ?
            `${REGISTER_URL}?aid=${affiliateId}` : REGISTER_URL;

        return Vue.http.post(registerUrl, credentials, REGISTER_HEADERS)
            .then(response => this.registerMixpanelUser(response.body));
    },

    registerMixpanelUser (user) {
        const mixpanelUser = {
            "$firstName": user.first_name,
            "$lastName":  user.last_name,
            "$name":      user.first_name + ' ' + user.last_name,
            "$created":   new Date(),
            "$email":     user.username,
            userId:       user.id,
            accountId:    user.id,
            accountName:  user.username
        };

        // set people properties
        mixpanel.people.set_once(mixpanelUser);
        mixpanel.ingage_instants.people.set_once(mixpanelUser);
        mixpanel.ingage_stories.people.set_once(mixpanelUser);

        // identify user
        mixpanel.identify(user.id);
        mixpanel.ingage_instants.identify(user.id);
        mixpanel.ingage_stories.identify(user.id);
    },

    setAuthHeader (request) {
        request.headers.set('Authorization', 'Bearer ' + store.state.auth.accessToken);
    },

    _retry (request) {
        this.setAuthHeader(request);

        return Vue.http(request)
            .then((response) => {
                return response
            })
            .catch((response) => {
                return response
            });
    },

    _refreshToken (request) {
        const params = {
            client_id:     CLIENT_ID,
            grant_type:    'refresh_token',
            refresh_token: store.state.auth.refreshToken
        };

        return Vue.http.post(LOGIN_URL, params, LOGIN_HEADERS)
            .then((result) => {
                this._storeToken(result);
                return this._retry(request);
            })
            .catch((errorResponse) => {
                if (this._isInvalidToken(errorResponse)) {
                    this.logout();
                }
                return errorResponse;
            });
    },

    _storeToken (response) {
        const { access_token, refresh_token, user } = response.body;

        const auth = {
            isLoggedIn:   true,
            accessToken:  access_token,
            refreshToken: refresh_token
        };

        store.commit('UPDATE_AUTH', auth);
        store.commit('UPDATE_USER', user);
    },

    _isInvalidToken (response) {
        return response.status === 401 && response.statusText === 'Unauthorized';
    }
};
