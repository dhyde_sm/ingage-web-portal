// dependencies
import { withParams } from 'vuelidate';

// value contains at least one digit
export const oneDigit = withParams({ type: 'oneDigit' }, value => /\d/.test(value));

// value contains at least one lower case character
export const oneLower = withParams({ type: 'oneLower' }, value => /[a-z]/.test(value));

// value contains at least one upper case character
export const oneCap = withParams({ type: 'oneCap' }, value => /[A-Z]/.test(value));

// value contains no spaces
export const noSpaces = withParams({ type: 'noSpaces' }, value => /^\S*$/.test(value));

// Stripe validation for credit card numbers
export const validCardNumber = withParams({ type: 'validCardNumber' }, value => Stripe.card.validateCardNumber(value));

// Stripe validation for card verification code
export const validCVC = withParams({ type: 'validCVC' }, value => Stripe.card.validateCVC(value));

// Stripe validation for card expiration date
export const validExpiry = withParams({ type: 'validExpiry' }, value => {
    const month = value && value.split('/')[0] && value.split('/')[0].trim();
    const year  = value && value.split('/')[1] && value.split('/')[1].trim();

    return Stripe.card.validateExpiry(month, year);
});
