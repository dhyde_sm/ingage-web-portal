// dependencies
import Vue from 'vue';

// api host name
import { HOST_NAME } from './host';

const MEMBERSHIPS_URL = HOST_NAME + '/memberships/';

export default {
    getMemberships () {
        return Vue.http.get(MEMBERSHIPS_URL)
            .then(response => response.body);
    }
};
