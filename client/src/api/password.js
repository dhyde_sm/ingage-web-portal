// dependencies
import Vue from 'vue';

// api host name
import { HOST_NAME } from './host';

const CHANGE_PASSWORD_URL = HOST_NAME + '/password/change/';

export default {
    changePassword (passwords) {
        return Vue.http.post(CHANGE_PASSWORD_URL, passwords);
    }
};
