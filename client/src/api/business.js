// dependencies
import Vue from 'vue';

// api host name
import { HOST_NAME } from './host';

const BUSINESS_URL = HOST_NAME + '/businesses/';

export default {
    createBusiness (businessDetails) {
        return Vue.http.post(BUSINESS_URL, businessDetails)
            .then(response => response.body);
    },

    getBusinessDetails (url) {
        return Vue.http.get(url)
            .then(response => response.body);
    },

    updateBusinessDetails (url, businessDetails) {
        return Vue.http.patch(url, businessDetails)
            .then(response => response.body);
    }
};
