// dependencies
import Vue from 'vue';

export default {
    getUserDetails (url) {
        return Vue.http.get(url)
            .then(response => response.body);
    },

    updateUserDetails (url, userDetails) {
        return Vue.http.patch(url, userDetails)
            .then(response => response.body);
    }
};
