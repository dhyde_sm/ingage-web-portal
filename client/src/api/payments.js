// dependencies
import Vue from 'vue';

// api host name
import { HOST_NAME } from './host';

const COUPON_URL = HOST_NAME + '/smpayments/coupons/';

const LISTING_PAYMENT_URL = HOST_NAME + '/smpayments/listing/subscribe/';

const LISTING_PROMO_URL = HOST_NAME + '/smpayments/listing/promo/';

const SUBSCRIPTION_PAYMENT_URL = HOST_NAME + '/smpayments/subscribe/';

const SUBSCRIPTION_PROMO_URL = HOST_NAME + '/smpayments/ingage/promo/';

export default {
    validateCoupon (couponCode) {
        return Vue.http.get(`${COUPON_URL}${couponCode}/`)
            .then(response => response.body);
    },

    createListingPayment (paymentDetails, affiliateId) {
        const listingPaymentUrl = affiliateId ?
            `${LISTING_PAYMENT_URL}?aid=${affiliateId}` : LISTING_PAYMENT_URL;

        return Vue.http.post(listingPaymentUrl, paymentDetails)
            .then(response => response.body);
    },

    freeListingPromo (paymentDetails) {
        return Vue.http.post(LISTING_PROMO_URL, paymentDetails)
            .then(response => response.body);
    },

    createSubscriptionPayment (paymentDetails, affiliateId) {
        const subscriptionPaymentUrl = affiliateId ?
            `${SUBSCRIPTION_PAYMENT_URL}?aid=${affiliateId}` : SUBSCRIPTION_PAYMENT_URL;

        return Vue.http.post(subscriptionPaymentUrl, paymentDetails)
            .then(response => response.body);
    },

    freeSubscriptionPromo (paymentDetails) {
        return Vue.http.post(SUBSCRIPTION_PROMO_URL, paymentDetails)
            .then(response => response.body);
    }
};
