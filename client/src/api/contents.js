// dependencies
import Vue from 'vue';

// api host name
import { HOST_NAME } from './host';

const CONTENTS_URL = HOST_NAME + '/contents/';

export default {
    getContents () {
        return Vue.http.get(CONTENTS_URL)
            .then(response => response.body);
    }
};
