// dependencies
import Vue from 'vue';

// api host name
import { HOST_NAME } from './host';

const LISTINGS_URL = HOST_NAME + '/listings/';

export default {
    createListing (listingDetails) {
        return Vue.http.post(LISTINGS_URL, listingDetails)
            .then(response => response.body);
    },

    getListings () {
        return Vue.http.get(LISTINGS_URL)
            .then(response => response.body);
    },

    updateListing (listingId, listingDetails) {
        return Vue.http.patch(`${LISTINGS_URL}${listingId}/`, listingDetails)
            .then(response => response.body);
    },

    getListingDetails (url) {
        return Vue.http.get(url)
            .then(response => response.body);
    }
}
