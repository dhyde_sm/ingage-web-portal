import Vue from 'vue';

// Capitalize
Vue.filter('capitalize', value => {
    if (!value) return '';
    value = value.toString();
    return value.charAt(0).toUpperCase() + value.slice(1);
});

// Format currency
Vue.filter('currency', amount => {
    return (amount / 100)
        .toLocaleString('en-US', { style: 'currency', currency: 'USD' });
});
