export default {
    setForgotPasswordEmail ({ commit }, email) {
        commit('SET_FORGOT_PASSWORD_EMAIL', email);
    },
    clearForgotPasswordEmail ({ commit }) {
        commit('CLEAR_FORGOT_PASSWORD_EMAIL');
    }
}
