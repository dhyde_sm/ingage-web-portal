export default {
    SET_FORGOT_PASSWORD_EMAIL (state, email) {
        state.email = email;
    },
    CLEAR_FORGOT_PASSWORD_EMAIL (state) {
        state.email = null;
    },
    CLEAR_ALL_DATA (state) {
        state.email = null;
    }
}
