export default {
    SET_AFFILIATE_ID (state, affiliateId) {
        state.affiliateId = affiliateId;
    },
    UPDATE_CONTENTS (state, contents) {
        state.contents = contents;
    },
    UPDATE_CONTENT (state, { id, details }) {
        const content = state.contents.find(content => content.id === id);

        if (content) {
            Object.assign(content, details);
        }
    },
    UPDATE_LISTINGS (state, listings) {
        state.listings = listings;
    },
    UPDATE_LISTING (state, { id, details }) {
        const listing = state.listings.find(listing => listing.id === id);

        if (listing) {
            Object.assign(listing, details);
        }
    },
    UPDATE_MEMBERSHIPS (state, memberships) {
        state.memberships = memberships;
    },
    SET_MEMBERSHIP_STATUS (state, { instantsProUser, storiesProUser }) {
        state.instantsProUser = instantsProUser;
        state.storiesProUser  = storiesProUser;
    },
    TOGGLE_EXPAND_HEADER (state) {
        state.headerExpanded = !state.headerExpanded;
    },
    SHOW_PURCHASE_CONFIRM (state) {
        state.purchaseConfirm = true;
    },
    HIDE_PURCHASE_CONFIRM (state) {
        state.purchaseConfirm = false;
    },
    CLEAR_ALL_DATA (state) {
        state.affiliateId     = null;
        state.contents        = [];
        state.listings        = [];
        state.memberships     = [];
        state.instantsProUser = false;
        state.storiesProUser  = false;
        state.headerExpanded  = false;
        state.purchaseConfirm = false;
    }
};
