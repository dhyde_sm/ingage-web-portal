export default {
    inProgress:   false,
    currentTier: {
        plan:      null,
        name:      null,
        frequency: null,
        slug:      null,
        price:     null
    },
    tiers: {
        listingPresentation: {
            plan:      'Listing Presentation',
            name:      'Ingage Listing Presentation',
            frequency: 'yearly',
            slug:      'listing_design_fee_yearly',
            price:     29900
        },
        instantsPro: {
            plan:      'Instants Pro',
            name:      'Instants Pro Yearly Subscription',
            frequency: 'yearly',
            slug:      'web_instants_pro_year',
            price:     2999
        },
        storiesProMonthly: {
            plan:      'Stories Pro',
            name:      'Stories Pro Monthly Subscription',
            frequency: 'monthly',
            slug:      'web_stories_pro_month',
            price:     1999
        },
        storiesProYearly: {
            plan:      'Stories Pro',
            name:      'Stories Pro Yearly Subscription',
            frequency: 'yearly',
            slug:      'web_stories_pro_year',
            price:     19999
        }
    },
    address: {
        name:    null,
        line1:   null,
        line2:   null,
        city:    null,
        state:   null,
        zip:     null,
        country: null
    },
    discount: {
        couponCode: null,
        amountOff:  null,
        percentOff: null
    },
    listingId: null
};
