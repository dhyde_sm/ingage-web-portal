export default {
    START_WORKFLOW_PROGRESS (state) {
        state.inProgress = true;
    },
    STOP_WORKFLOW_PROGRESS (state) {
        state.inProgress = false;
    },
    SET_CURRENT_TIER (state, currentTier) {
        state.currentTier = {
            plan:      currentTier.plan,
            name:      currentTier.name,
            frequency: currentTier.frequency,
            slug:      currentTier.slug,
            price:     currentTier.price
        };
    },
    CLEAR_CURRENT_TIER (state) {
        state.currentTier = {
            plan:      null,
            name:      null,
            frequency: null,
            slug:      null,
            price:     null
        };
    },
    SET_ADDRESS (state, address) {
        state.address = {
            name:    address.name,
            line1:   address.line1,
            line2:   address.line2,
            city:    address.city,
            state:   address.state,
            zip:     address.zip,
            country: address.country
        };
    },
    UPDATE_ADDRESS_NAME (state, name) {
        state.address.name = name;
    },
    CLEAR_ADDRESS (state) {
        state.address = {
            name:    null,
            line1:   null,
            line2:   null,
            city:    null,
            state:   null,
            zip:     null,
            country: null
        };
    },
    SET_DISCOUNT (state, { couponCode, amount_off, percent_off }) {
        state.discount.couponCode = couponCode;
        state.discount.amountOff  = amount_off;
        state.discount.percentOff = percent_off;
    },
    CLEAR_DISCOUNT (state) {
        state.discount = {
            couponCode: null,
            amountOff:  null,
            percentOff: null
        };
    },
    SET_LISTING_ID (state, listingId) {
        state.listingId = listingId;
    },
    CLEAR_LISTING_ID (state) {
        state.listingId = null;
    },
    CLEAR_ALL_DATA (state) {
        state.inProgress = false;
        state.listingId  = null;

        state.currentTier = {
            plan:      null,
            name:      null,
            frequency: null,
            slug:      null,
            price:     null
        };

        state.discount = {
            couponCode: null,
            amountOff:  null,
            percentOff: null
        };

        state.address = {
            name:    null,
            line1:   null,
            line2:   null,
            city:    null,
            state:   null,
            zip:     null,
            country: null
        };
    }
};
