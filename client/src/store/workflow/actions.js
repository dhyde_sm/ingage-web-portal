// api
import payments from '@/api/payments';

export default {
    startWorkflow ({ state, rootState, commit }, tier) {
        commit('START_WORKFLOW_PROGRESS');

        const { appMode } = rootState;
        const { tiers }   = state;

        // set tier if it exists
        if (tier && tiers[tier]) {
            commit('SET_CURRENT_TIER', tiers[tier]);
        }

        // if ingage mode, default to instants
        else if (appMode === 'ingage') {
            commit('SET_CURRENT_TIER', tiers.instantsPro);
        }

        // otherwise default to listing presentation
        else {
            commit('SET_CURRENT_TIER', tiers.listingPresentation);
        }
    },
    stopWorkflow ({ commit }) {
        commit('STOP_WORKFLOW_PROGRESS');

        commit('CLEAR_CURRENT_TIER');

        commit('CLEAR_ADDRESS');

        commit('CLEAR_DISCOUNT');

        commit('CLEAR_LISTING_ID');
    },
    setCurrentTier ({ commit }, tier) {
        commit('SET_CURRENT_TIER', tier);
    },
    setAddress ({ commit }, address) {
        commit('SET_ADDRESS', address);
    },
    updateAddressName ({ commit }, name) {
        commit('UPDATE_ADDRESS_NAME', name);
    },
    setDiscount ({ commit }, discount) {
        commit('SET_DISCOUNT', discount);
    },
    setListingId ({ commit }, listingId) {
        commit('SET_LISTING_ID', listingId);
    },
    purchase ({ rootState, dispatch }, cardInfo) {
        const { appMode } = rootState;

        if (appMode === 'realestate') {
            return dispatch('purchaseListing', cardInfo);
        }

        return dispatch('purchaseSubscription', cardInfo);
    },
    purchaseListing ({ state, dispatch }, cardInfo) {
        const { listingId, address } = state;

        // update name if necessary
        if (cardInfo.nameOnCard !== address.name) {
            dispatch('updateAddressName', cardInfo.nameOnCard);
        }

        if (listingId) {
            return dispatch('createListingPayment', cardInfo);
        }

        return dispatch('createListing')
            .then(listingId => dispatch('setListingId', listingId))
            .then(() => dispatch('createListingPayment', cardInfo));
    },
    purchaseSubscription ({ rootState, state, dispatch }, cardInfo) {
        const { affiliateId }         = rootState;
        const { currentTier, discount } = state;
        const { couponCode }            = discount;

        return dispatch('createToken', cardInfo)
            .then(stripeToken => {
                return new Promise((resolve, reject) => {
                    payments.createSubscriptionPayment({
                        slug:   currentTier.slug,
                        token:  stripeToken,
                        coupon: couponCode
                    }, affiliateId)
                    .then(resolve)
                    .catch(({ body }) => {
                        reject(body.error || 'There was a problem processing your payment');
                    });
                });
            });
    },
    createListingPayment ({ rootState, state, dispatch }, cardInfo) {
        const { affiliateId } = rootState;
        const { listingId }   = state;

        return dispatch('createToken', cardInfo)
            .then(stripeToken => {
                return new Promise((resolve, reject) => {
                    payments.createListingPayment({
                        token:   stripeToken,
                        listing: listingId
                    }, affiliateId)
                    .then(resolve)
                    .catch(({ body }) => {
                        reject(body.error || 'There was a problem processing your payment');
                    });
                });
            });
    },
    createToken ({ state }, cardInfo) {
        const {
            number,
            cvc,
            expMonth,
            expYear
        } = cardInfo;

        const { address } = state;

        return new Promise((resolve, reject) => {
            Stripe.card.createToken({
                number,
                cvc,
                exp_month:       expMonth,
                exp_year:        expYear,
                name:            address.name,
                address_line1:   address.line1,
                address_line2:   address.line2,
                address_city:    address.city,
                address_state:   address.state,
                address_zip:     address.zip,
                address_country: address.country
            }, (status, response) => {
                if (response.error) {
                    reject({ message: response.error.message });
                }

                else {
                    resolve(response.id);
                }
            });
        });
    },
}
