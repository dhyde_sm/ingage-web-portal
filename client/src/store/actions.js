// api
import listings    from '@/api/listings';
import contents    from '@/api/contents';
import memberships from '@/api/memberships';

// utils
import { formatDate } from '../utils';

export default {
    setAffiliateId ({ commit }, affiliateId) {
        commit('SET_AFFILIATE_ID', affiliateId);
    },

    showPurchaseConfirm ({ commit }) {
        commit('SHOW_PURCHASE_CONFIRM');
    },

    hidePUrchaseConfirm ({ commit }) {
        commit('HIDE_PURCHASE_CONFIRM');
    },

    createListing (context) {
        const listingDetails = {
            name: "'Listing presentation' coming soon"
        };

        return listings.createListing(listingDetails)
            .then(({ id }) => id);
    },

    getListings ({ commit }) {
        return listings.getListings()
            .then(listings =>
                listings
                    .filter(listing => listing.status !== 'complete')
                    .map(listing => {
                        const { memberships } = listing;

                        // time listing was paid for
                        const created = memberships[0] && memberships[0].created_at;

                        return {
                            id:      listing.id,
                            name:    listing.name,
                            image:   listing.image,
                            status:  listing.status,
                            created: formatDate(created)
                        };
                    })
            )
            .then(listings => commit('UPDATE_LISTINGS', listings));
    },

    updateListing ({ commit }, { listingId, listingDetails }) {
        return listings.updateListing(listingId, listingDetails)
            .then(listing => {
                const { memberships } = listing;

                // time listing was paid for
                const created = memberships[0] && memberships[0].created_at;

                const details = {
                    id:      listing.id,
                    name:    listing.name,
                    image:   listing.image,
                    status:  listing.status,
                    created: formatDate(created)
                }

                commit('UPDATE_LISTING', { id: listing.id, details });
            });
    },

    getContents ({ commit, dispatch }) {
        return contents.getContents()
            .then(contents =>
                contents
                .filter(content => content.share_type === 'public')
                .map(content => {
                    const latestVersion = content.versions[0] || {};

                    if (content.listing) {
                        dispatch('getListingDetails', {
                            id:        content.id,
                            url: content.listing
                        });
                    }

                    return {
                        id:        content.id,
                        url:       content.share_url,
                        name:      latestVersion.name,
                        thumbnail: latestVersion.thumbnail,
                        expires:   null,
                        status:    null
                    };
                })
            )
            .then(contents => commit('UPDATE_CONTENTS', contents));
    },

    getListingDetails ({ state, commit }, { id, url }) {
        return listings.getListingDetails(url)
            .then(listing => {
                const membership = listing.memberships[0] || {};

                const details = {
                    expires: formatDate(membership.end_at),
                    status:  membership.status
                };

                commit('UPDATE_CONTENT', { id, details });
            });
    },

    getMemberships ({ commit, dispatch }) {
        return memberships.getMemberships()
            .then(memberships => {
                commit('UPDATE_MEMBERSHIPS', memberships);

                dispatch('checkMembershipStatus', memberships);
            });
    },

    checkMembershipStatus ({ commit }, memberships) {
        const instantsProUser = !!(memberships.find(membership => {
            const { status, tier } = membership;

            return (tier.slug === 'plus' ||
                    tier.slug === 'web_instants_pro_year')
                && (status === 'active' ||
                    status === 'provisional');
        }));

        const storiesProUser = !!(memberships.find(membership => {
            const { status, tier } = membership;

            return (tier.slug === 'paid' ||
                    tier.slug === 'pro' ||
                    tier.slug === 'web_paid_monthly' ||
                    tier.slug === 'web_paid_yearly' ||
                    tier.slug === 'listing_design_fee_yearly' ||
                    tier.slug === 'web_pro_monthly' ||
                    tier.slug === 'web_pro_yearly' ||
                    tier.slug === 'web_stories_pro_month' ||
                    tier.slug === 'web_stories_pro_year')
                && (status === 'active' ||
                    status === 'provisional');
        }));

        commit('SET_MEMBERSHIP_STATUS', { instantsProUser, storiesProUser });
    }
};
