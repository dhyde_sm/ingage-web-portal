// dependencies
import Vue  from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

// state
import state from './state';

// actions
import actions from './actions';

// mutations
import mutations from './mutations';

// store modules
import auth     from './auth';
import user     from './user';
import business from './business';
import workflow from './workflow';
import forgot   from './forgot';

// plugins
import plugins from './plugins';

export default new Vuex.Store({
    state,
    actions,
    mutations,
    modules: {
        auth,
        user,
        business,
        workflow,
        forgot
    },
    plugins
});
