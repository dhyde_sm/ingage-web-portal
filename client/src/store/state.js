import { STORAGE_KEY } from './plugins';

// default state
let state = {
    appMode:         window.dashboardConfig.APP_MODE || 'ingage',
    affiliateId:     null,
    contents:        [],
    listings:        [],
    memberships:     [],
    instantsProUser: false,
    storiesProUser:  false,
    headerExpanded:  false,
    purchaseConfirm: false,
};

// stored state
if (localStorage.getItem(STORAGE_KEY)) {
    const { affiliateId } = JSON.parse(localStorage.getItem(STORAGE_KEY));

    state.affiliateId = affiliateId;
}

export default state;
