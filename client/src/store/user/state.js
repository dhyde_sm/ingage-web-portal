import { STORAGE_KEY }  from '../plugins';

// default state
let state = {
    id:          null,
    url:         null,
    firstName:   null,
    lastName:    null,
    email:       null,
    photo:       null,
    hasBusiness: false,
    businesses:  null
};

// stored state
if (localStorage.getItem(STORAGE_KEY)) {
    const { user } = JSON.parse(localStorage.getItem(STORAGE_KEY));

    state = Object.assign(state, user);
}

export default state;
