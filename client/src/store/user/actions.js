import user from '../../api/user';

export default {
    getUserDetails ({ state, commit }) {
        if (state.url) {
            return user.getUserDetails(state.url)
                .then(userDetails => {
                    // identify user
                    mixpanel.identify(userDetails.id);
                    mixpanel.ingage_instants.identify(userDetails.id);
                    mixpanel.ingage_stories.identify(userDetails.id);

                    commit('SET_USER_DETAILS', {
                        email:       userDetails.email,
                        firstName:   userDetails.first_name,
                        lastName:    userDetails.last_name,
                        photo:       userDetails.photo,
                        businesses:  userDetails.businesses,
                        hasBusiness: userDetails.businesses > 0
                    });
                });
        }
    },

    updateUserDetails ({ state, commit }, userDetails) {
        if (state.url) {
            return user.updateUserDetails(state.url, userDetails)
                .then(userDetails => {
                    commit('SET_USER_DETAILS', {
                        email:       userDetails.email,
                        firstName:   userDetails.first_name,
                        lastName:    userDetails.last_name,
                        photo:       userDetails.photo,
                        businesses:  userDetails.businesses,
                        hasBusiness: userDetails.businesses > 0
                    });
                });
        }
    }
};
