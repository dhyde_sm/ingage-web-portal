export default {
    UPDATE_USER (state, { id, url }) {
        state.id  = id;
        state.url = url;
    },
    SET_USER_DETAILS (state, userDetails) {
        state.email       = userDetails.email;
        state.firstName  = userDetails.firstName;
        state.lastName   = userDetails.lastName;
        state.photo       = userDetails.photo;
        state.businesses  = userDetails.businesses;
        state.hasBusiness = userDetails.hasBusiness
    },
    CLEAR_ALL_DATA (state) {
        state.id          = null;
        state.url         = null;
        state.email       = null;
        state.firstName   = null;
        state.lastName    = null;
        state.photo       = null;
        state.businesses  = null;
        state.hasBusiness = false;
    }
};
