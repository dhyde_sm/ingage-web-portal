import { STORAGE_KEY }  from '../plugins';

// default state
let state = {
    isLoggedIn:   false,
    accessToken:  null,
    refreshToken: null
};

// stored state
if (localStorage.getItem(STORAGE_KEY)) {
    const { auth } = JSON.parse(localStorage.getItem(STORAGE_KEY));

    state = Object.assign(state, auth);
}

export default state;
