export default {
    UPDATE_AUTH (state, { isLoggedIn, accessToken, refreshToken }) {
        state.isLoggedIn   = isLoggedIn;
        state.accessToken  = accessToken;
        state.refreshToken = refreshToken;
    },
    CLEAR_ALL_DATA (state) {
        state.isLoggedIn   = false;
        state.accessToken  = null;
        state.refreshToken = null;
    }
};
