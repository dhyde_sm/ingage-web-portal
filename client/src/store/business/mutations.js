export default {
    SET_BUSINESS_DETAILS (state, businessDetails) {
        state.name        = businessDetails.name;
        state.phoneNumber = businessDetails.phoneNumber;
        state.website     = businessDetails.website;
        state.description = businessDetails.description
    },
    CLEAR_ALL_DATA (state) {
        state.name        = null;
        state.phoneNumber = null;
        state.website     = null;
        state.description = null;
    }
};
