import business from '../../api/business';

export default {
    createBusiness ({ dispatch }, businessDetails) {
        return business.createBusiness(businessDetails)
            .then(response => dispatch('setBusinessDetails', response));
    },

    getBusinessDetails ({ rootState, dispatch }) {
        const { businesses } = rootState.user;
        const url = businesses && businesses[0] && businesses[0].url;

        if (!url) {
            return Promise.resolve();
        }

        return business.getBusinessDetails(url)
            .then(response => dispatch('setBusinessDetails', response));
    },

    updateBusinessDetails ({ rootState, dispatch }, businessDetails) {
        const { hasBusiness, businesses } = rootState.user;

        if (!hasBusiness) {
            return dispatch('createBusiness', businessDetails);
        }

        const url = businesses[0].url;

        return business.updateBusinessDetails(url, businessDetails)
            .then(response => dispatch('setBusinessDetails', response));
    },

    setBusinessDetails ({ commit }, businessDetails) {
        commit('SET_BUSINESS_DETAILS', {
            name:        businessDetails.name,
            phoneNumber: businessDetails.phone_number,
            website:     businessDetails.website,
            description: businessDetails.description
        });
    }
};
