import { STORAGE_KEY }  from '../plugins';

// default state
let state = {
    name:        null,
    phoneNumber: null,
    website:     null,
    description: null
};

// stored state
if (localStorage.getItem(STORAGE_KEY)) {
    const { business } = JSON.parse(localStorage.getItem(STORAGE_KEY));

    state = Object.assign(state, business);
}

export default state;
