export const STORAGE_KEY = 'real-estate-dashboard';

const localStoragePlugin = store => {
    store.subscribe((mutation, state) => {
        const {
            affiliateId,
            auth,
            user,
            business
        } = state;

        localStorage.setItem(STORAGE_KEY, JSON.stringify({
            affiliateId,
            auth,
            user,
            business
        }));

        if (mutation.type === 'CLEAR_ALL_DATA') {
            localStorage.removeItem(STORAGE_KEY);
        }
    });
};

export default [localStoragePlugin];
