import Vue from 'vue';

// Custom directive for element focus
Vue.directive('focus', {
    inserted (el) {
        el.focus();
    }
});

// Custom directive for visibility: hidden
Vue.directive('visible', (el, binding) => {
    const { value } = binding;

    if (!!value) {
        el.style.visibility = 'visible';
    }

    else {
        el.style.visibility = 'hidden';
    }
});
