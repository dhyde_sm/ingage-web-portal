// dependencies
import Vue         from 'vue';
import VueResource from 'vue-resource';
import Vuelidate   from 'vuelidate';

// store
import store from './store';

// router
import router from './router';

// components
import App from './App.vue';

// authentication helper
import auth from './auth';

// directives
import './directives';

// filters
import './filters';

Vue.use(VueResource);

Vue.use(Vuelidate);

Vue.use(auth);

// init app
new Vue({
    el: '#app',
    store,
    router,
    render: h => h(App)
})
