// components
import DashboardHeader from '@/components/dashboard/DashboardHeader.vue';
import AddressForm from '@/components/address/AddressForm.vue';

// router guard
import { notLoggedInGuard } from '@/router/guards';

export default {
    path: '/address',
    name: 'address',
    components:  {
        header: DashboardHeader,
        default: AddressForm
    },
    beforeEnter: notLoggedInGuard
};
