// dependencies
import Vue    from 'vue';
import Router from 'vue-router';

// store
import store from '@/store';

// routes
import dashboard from './dashboard';

import loginInstants from './login/loginInstants';
import loginStories from './login/loginStories';
import loginRealestate from './login/loginRealestate';

import registerInstants from './register/registerInstants';
import registerStories from './register/registerStories';
import registerRealestate from './register/registerRealestate';

import registerSuccessInstants from './register/registerSuccessInstants';
import registerSuccessStories from './register/registerSuccessStories';

import address from './address';
import payment from './payment';

import purchaseInstants from './purchase/purchaseInstants';
import purchaseStories from './purchase/purchaseStories';

import purchaseSuccessInstants from './purchase/purchaseSuccessInstants';
import purchaseSuccessStories from './purchase/purchaseSuccessStories';

import subscribedInstants from './subscribed/subscribedInstants';
import subscribedStories from './subscribed/subscribedStories';

import forgotInstants from './forgot/forgotInstants';
import forgotStories from './forgot/forgotStories';
import forgotRealestate from './forgot/forgotRealestate';

import resetInstants from './reset/resetInstants';
import resetStories from './reset/resetStories';
import resetRealestate from './reset/resetRealestate';

Vue.use(Router);

const appMode = store.state.appMode;

let routes;

if (appMode === 'ingage') {
    routes = [
        {
            path: '/',
            name: 'default',
            redirect: { name: 'purchase-stories' }
        },

        // purchase form
        // purchaseInstants,
        {
            path: '/instants/purchase',
            redirect: { name: 'purchase-stories-monthly' }
        },
        purchaseStories,

        // purchase success
        // purchaseSuccessInstants,
        {
            path: '/instants/purchase/success',
            redirect: { name: 'purchase-success-stories' }
        },
        purchaseSuccessStories,

        // user is subscribed
        // subscribedInstants,
        {
            path: '/instants/purchase/subscribed',
            redirect: { name: 'purchase-subscribed-stories' }
        },
        subscribedStories,

        // login form
        // loginInstants,
        {
            path: '/instants/login',
            redirect: { name: 'login-stories' }
        },
        loginStories,

        // register form
        // registerInstants,
        {
            path: '/instants/register',
            redirect: { name: 'register-stories' }
        },
        registerStories,

        // register success
        // registerSuccessInstants,
        {
            path: '/instants/register/success',
            redirect: { name: 'register-success-stories' }
        },
        registerSuccessStories,

        // forgot password
        // forgotInstants,
        {
            path: '/instants/forgot',
            redirect: { name: 'forgot-stories' }
        },
        forgotStories,

        // reset password
        // resetInstants,
        {
            path: '/instants/reset',
            redirect: { name: 'reset-stories' }
        },
        resetStories,

        // prevent 404s
        { path: '*', redirect: '/' }
    ];
}

else {
    routes = [
        {
            path: '/',
            name: 'default',
            redirect: { name: 'address' }
        },

        // dashboard
        dashboard,

        // login form
        loginRealestate,

        // register form
        registerRealestate,

        // purchase listing presentation workflow
        address,
        payment,

        // forgot password workflow
        forgotRealestate,
        resetRealestate,

        // prevent 404s
        { path: '*', redirect: '/' }
    ];
}

const router = new Router({
    base: `/${appMode}/`,
    mode: 'history',
    routes
});

router.beforeEach((to, from, next) => {
    const { aid: affiliateId } = to.query;

    // Set affiliate ID
    if (affiliateId) {
        store.dispatch('setAffiliateId', affiliateId);
    }

    // Mixpanel page view event
    // if (to.name) {
        // mixpanel.track(`view-${to.name}-page`);
    // }

    next();
});

export default router;
