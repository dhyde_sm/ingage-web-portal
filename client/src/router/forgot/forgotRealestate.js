// components
import ForgotPassword from '@/components/ForgotPassword.vue';

export default {
    path: '/forgot-password',
    name: 'forgot-password',
    component: ForgotPassword
};
