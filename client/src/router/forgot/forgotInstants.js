// components
import ForgotPassword from '@/components/ForgotPassword.vue';

export default {
    path: '/instants/forgot',
    name: 'forgot-instants',
    component: ForgotPassword,
    meta: {
        reset: 'reset-instants'
    }
};
