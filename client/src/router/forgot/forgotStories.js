// components
import ForgotPassword from '@/components/ForgotPassword.vue';

export default {
    path: '/stories/forgot',
    name: 'forgot-stories',
    component: ForgotPassword,
    meta: {
        reset: 'reset-stories'
    }
};
