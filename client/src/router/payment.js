// router guard
import { inProgressGuard } from '@/router/guards';

// components
import DashboardHeader from '@/components/dashboard/DashboardHeader.vue';
import PaymentForm from '@/components/payment/PaymentForm.vue';

export default {
    path:        '/payment',
    name:        'payment',
    components: {
        header: DashboardHeader,
        default: PaymentForm
    },
    beforeEnter: inProgressGuard
};
