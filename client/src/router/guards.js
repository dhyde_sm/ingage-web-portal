// dependencies
import store from '@/store';

// redirect to content if workflow is not in progress
export const inProgressGuard = (to, from, next) => {
    const { auth, workflow } = store.state;

    if (!workflow.inProgress) {
        const fallbackRoute = auth.isLoggedIn ? 'default' : 'login';

        next({ name: fallbackRoute });
    }

    else {
        next();
    }
};

export const notLoggedInGuard = (to, from, next) => {
    const { auth } = store.state;

    const registerRoute = to.meta.register || 'register';

    // Redirect to register page
    // if user is not logged in
    if (!auth.isLoggedIn) {
        next({
            name:  registerRoute,
            query: { redirect: to.name }
        });
    }

    else {
        next();
    }
};

export const alreadyLoggedInGuard = (to, from, next) => {
    const { auth } = store.state;

    // redirect if user is already logged in
    if (auth.isLoggedIn) {
        const redirectRoute = to.query.redirect || to.meta.default;

        next({ name: redirectRoute });
    }

    else {
        next();
    }
};
