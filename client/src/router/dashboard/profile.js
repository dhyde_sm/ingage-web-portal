// components
import DashboardProfile from '@/components/dashboard/profile/DashboardProfile.vue';

export default {
    path:      '/profile',
    name:      'profile',
    component: DashboardProfile
};
