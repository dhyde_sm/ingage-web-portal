// components
import DashboardContent from '@/components/dashboard/content/DashboardContent.vue';

export default {
    path:      '/content',
    name:      'content',
    component: DashboardContent
};
