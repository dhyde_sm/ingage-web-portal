// components
import DashboardHeader from '@/components/dashboard/DashboardHeader.vue';
import Dashboard       from '@/components/dashboard/Dashboard.vue';

// child routes
import content from './content';
import profile from './profile';

// router guard
import { notLoggedInGuard } from '@/router/guards';

export default {
    path: '/',
    name: 'dashboard',
    components: {
        header:  DashboardHeader,
        default: Dashboard
    },
    redirect: { name: 'content' },
    beforeEnter: notLoggedInGuard,
    children: [
        content,
        profile
    ]
};
