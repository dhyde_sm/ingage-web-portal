// components
import ResetPassword from '@/components/ResetPassword.vue';

export default {
    path: '/instants/reset',
    name: 'reset-instants',
    component: ResetPassword,
    meta: {
        login: 'login-instants'
    }
};
