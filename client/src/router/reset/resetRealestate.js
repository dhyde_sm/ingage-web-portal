// components
import ResetPassword from '@/components/ResetPassword.vue';

export default {
    path: '/reset-password',
    name: 'reset-password',
    component: ResetPassword
};
