// components
import ResetPassword from '@/components/ResetPassword.vue';

export default {
    path: '/stories/reset',
    name: 'reset-stories',
    component: ResetPassword,
    meta: {
        login: 'login-stories'
    }
};
