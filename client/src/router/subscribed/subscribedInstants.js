// router guard
import { notLoggedInGuard } from '@/router/guards';

// components
import PurchaseHeader from '@/components/purchase/PurchaseHeader.vue';
import Subscribed     from '@/components/subscribed/Subscribed.vue';

export default {
    path: '/instants/purchase/subscribed',
    name: 'purchase-subscribed-instants',
    components: {
        header:  PurchaseHeader,
        default: Subscribed
    },
    beforeEnter: notLoggedInGuard,
    meta: {
        login: 'login-instants',
        register: 'register-instants'
    }
};
