// router guard
import { notLoggedInGuard } from '@/router/guards';

// components
import PurchaseHeader from '@/components/purchase/PurchaseHeader.vue';
import Subscribed     from '@/components/subscribed/Subscribed.vue';

export default {
    path: '/stories/purchase/subscribed',
    name: 'purchase-subscribed-stories',
    components: {
        header:  PurchaseHeader,
        default: Subscribed
    },
    beforeEnter: notLoggedInGuard,
    meta: {
        login: 'login-stories',
        register: 'register-stories'
    }
};
