// components
import LoginRealestate from '@/components/login/LoginRealestate.vue';

// router guard
import { alreadyLoggedInGuard } from '@/router/guards';

export default {
    path: '/login',
    name: 'login',
    component: LoginRealestate,
    beforeEnter: alreadyLoggedInGuard,
    meta: {
        default: 'default'
    }
};
