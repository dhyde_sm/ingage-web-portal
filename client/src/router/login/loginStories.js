// components
import LoginIngage from '@/components/login/LoginIngage.vue';

// router guard
import { alreadyLoggedInGuard } from '@/router/guards';

export default {
    path: '/stories/login',
    name: 'login-stories',
    component: LoginIngage,
    beforeEnter: alreadyLoggedInGuard,
    meta: {
        forgot: 'forgot-stories',
        register: 'register-stories',
        purchase: 'purchase-stories-monthly',
        default: 'purchase-stories'
    }
};
