// components
import LoginIngage from '@/components/login/LoginIngage.vue';

// router guard
import { alreadyLoggedInGuard } from '@/router/guards';

export default {
    path: '/instants/login',
    name: 'login-instants',
    component: LoginIngage,
    beforeEnter: alreadyLoggedInGuard,
    meta: {
        forgot: 'forgot-instants',
        register: 'register-instants',
        purchase: 'purchase-instants',
        default: 'purchase-instants'
    }
};
