// router guard
import { notLoggedInGuard } from '@/router/guards';

// components
import PurchaseHeader from '@/components/purchase/PurchaseHeader.vue';
import Success        from '@/components/success/Success.vue';

export default {
    path: '/instants/purchase/success',
    name: 'purchase-success-instants',
    components: {
        header:  PurchaseHeader,
        default: Success
    },
    beforeEnter: notLoggedInGuard,
    meta: {
        login: 'login-instants',
        register: 'register-instants'
    }
};
