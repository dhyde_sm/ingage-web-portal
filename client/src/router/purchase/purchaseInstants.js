// store
import store from '@/store';

// components
import PurchaseHeader from '@/components/purchase/PurchaseHeader.vue';
import Purchase from '@/components/purchase/Purchase.vue';

// router guard
import { notLoggedInGuard } from '@/router/guards';

export default {
    path: '/instants/purchase',
    name: 'purchase-instants',
    components: {
        header: PurchaseHeader,
        default: Purchase
    },
    beforeEnter: (to, from, next) => {
        store.dispatch('startWorkflow', 'instantsPro');

        notLoggedInGuard(to, from, next);
    },
    meta: {
        tier: 'instants',
        login: 'login-instants',
        register: 'register-instants',
        success: 'purchase-success-instants'
    }
};
