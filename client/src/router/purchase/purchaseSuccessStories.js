// router guard
import { notLoggedInGuard } from '@/router/guards';

// components
import PurchaseHeader from '@/components/purchase/PurchaseHeader.vue';
import Success        from '@/components/success/Success.vue';

export default {
    path: '/stories/purchase/success',
    name: 'purchase-success-stories',
    components: {
        header:  PurchaseHeader,
        default: Success
    },
    beforeEnter: notLoggedInGuard,
    meta: {
        login: 'login-stories',
        register: 'register-stories'
    }
};
