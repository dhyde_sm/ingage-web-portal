// store
import store from '@/store';

// router guard
import { notLoggedInGuard } from '@/router/guards';

export default {
    path: 'yearly',
    name: 'purchase-stories-yearly',
    beforeEnter: (to, from, next) => {
        store.dispatch('startWorkflow', 'storiesProYearly');

        notLoggedInGuard(to, from, next);
    },
    meta: {
        tier: 'stories',
        login: 'login-stories',
        register: 'register-stories',
        success: 'purchase-success-stories'
    }
};
