// store
import store from '@/store';

// router guard
import { notLoggedInGuard } from '@/router/guards';

export default {
    path: 'monthly',
    name: 'purchase-stories-monthly',
    beforeEnter: (to, from, next) => {
        store.dispatch('startWorkflow', 'storiesProMonthly');

        notLoggedInGuard(to, from, next);
    },
    meta: {
        tier: 'stories',
        login: 'login-stories',
        register: 'register-stories',
        success: 'purchase-success-stories'
    }
};
