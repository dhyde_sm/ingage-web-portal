// components
import PurchaseHeader from '@/components/purchase/PurchaseHeader.vue';
import Purchase from '@/components/purchase/Purchase.vue';

// child routes
import monthly from './monthly';
import yearly from './yearly';

export default {
    path: '/stories/purchase',
    name: 'purchase-stories',
    redirect: { name: 'purchase-stories-monthly' },
    components: {
        header: PurchaseHeader,
        default: Purchase
    },
    children: [
        monthly,
        yearly
    ]
};
