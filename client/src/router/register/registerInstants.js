// components
import RegisterIngage from '@/components/register/RegisterIngage.vue';

// router guard
import { alreadyLoggedInGuard } from '@/router/guards';

export default {
    path: '/instants/register',
    name: 'register-instants',
    component: RegisterIngage,
    beforeEnter: alreadyLoggedInGuard,
    meta: {
        login: 'login-instants',
        success: 'register-success-instants',
        default: 'purchase-instants'
    }
};
