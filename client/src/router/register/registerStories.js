// components
import RegisterIngage from '@/components/register/RegisterIngage.vue';

// router guard
import { alreadyLoggedInGuard } from '@/router/guards';

export default {
    path: '/stories/register',
    name: 'register-stories',
    component: RegisterIngage,
    beforeEnter: alreadyLoggedInGuard,
    meta: {
        login: 'login-stories',
        success: 'register-success-stories',
        default: 'purchase-stories'
    }
};
