// components
import RegisterRealestate from '@/components/register/RegisterRealestate.vue';

// router guard
import { alreadyLoggedInGuard } from '@/router/guards';

export default {
    path: '/register',
    name: 'register',
    component: RegisterRealestate,
    beforeEnter: alreadyLoggedInGuard,
    meta: {
        default: 'default'
    }
};
