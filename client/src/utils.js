// formats date string into mm/dd/yyyy
export const formatDate = dateString => {
    if (!dateString) return;

    const date  = new Date(dateString);
    const month = date.getMonth() + 1;
    const day   = date.getDate();
    const year  = date.getFullYear();

    return `${month}/${day}/${year}`;
};
