export default [
    {
        value: null,
        display: 'Industry',
        attributes: {
            disabled: true,
            selected: true
        }
    },
    {
        value: 'Architecture & Construction',
        display: 'Architecture & Construction'
    },
    {
        value: 'Arts & Entertainment',
        display: 'Arts & Entertainment'
    },
    {
        value: 'Beauty & Cosmetics',
        display: 'Beauty & Cosmetics'
    },
    {
        value: 'Clothing & Fashion',
        display: 'Clothing & Fashion'
    },
    {
        value: 'Consulting',
        display: 'Consulting'
    },
    {
        value: 'Education',
        display: 'Education'
    },
    {
        value: 'Enterprise',
        display: 'Enterprise'
    },
    {
        value: 'Finance',
        display: 'Finance'
    },
    {
        value: 'Food & Beverage',
        display: 'Food & Beverage'
    },
    {
        value: 'Health & Fitness',
        display: 'Health & Fitness'
    },
    {
        value: 'Hospitality',
        display: 'Hospitality'
    },
    {
        value: 'Marketing',
        display: 'Marketing'
    },
    {
        value: 'Non-profit',
        display: 'Non-profit'
    },
    {
        value: 'Real Estate',
        display: 'Real Estate'
    },
    {
        value: 'Retail',
        display: 'Retail'
    },
    {
        value: 'Sports',
        display: 'Sports'
    },
    {
        value: 'Travel',
        display: 'Travel'
    },
    {
        value: 'Other',
        display: 'Other'
    }
];
