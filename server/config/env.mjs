let env;

switch (process.env.NODE_ENV) {
    case 'production':
        env = {
            API_URL: 'https://ssc-api-prod.scrollmotion.com',
            CLIENT_ID: 'U13ewq4rVCkZwO8CE7Ni0QnumUakPNs1mY28BBvC',
            STRIPE_KEY: 'pk_live_YFQ8f79SvSww5RwZUkz6xoXo',
            GOOGLE_LOGIN_KEY: '636931751579-f7m3fhqrc065ulokn0a92okl9r8j1q1d.apps.googleusercontent.com',
            GOOGLE_ANALYTICS_KEY: 'UA-20319875-3',
            MIXPANEL_WEB_KEY: 'a6ff1e55986d03d463aad50a1d096594',
            MIXPANEL_INSTANTS_KEY: '569d2bd2ed029ca17e1efa49da08c351',
            MIXPANEL_STORIES_KEY: 'eb30ed4f3f61597ee0c360c3016cdc71'
        };
        break;
    case 'staging':
        env = {
            API_URL: 'https://ssc-api-qa.scrollmotion.com',
            CLIENT_ID: '1tmA4Xme0NnEws5WQ5m64fGUx8uMOYyCpUpAEAn2',
            STRIPE_KEY: 'pk_test_GlKgm5u2E280cFtYhvr5zR5u',
            GOOGLE_LOGIN_KEY: '636931751579-f7m3fhqrc065ulokn0a92okl9r8j1q1d.apps.googleusercontent.com',
            GOOGLE_ANALYTICS_KEY: 'UA-20319875-9',
            MIXPANEL_WEB_KEY: 'd51db9fe1de7a6d50989072372ff6fd5',
            MIXPANEL_INSTANTS_KEY: 'cc5a187cf67d7468c92c369d0bd340bb',
            MIXPANEL_STORIES_KEY: '7e89ee5c33b454c791c714b7a0b7e56f'
        };
        break;
    default:
        env = {
            API_URL: 'https://ssc-api-dev.scrollmotion.com',
            CLIENT_ID: 'obOaSNh6zGE70pJfRGdJHMck5MYL5JocsXLloRnR',
            STRIPE_KEY: 'pk_test_GlKgm5u2E280cFtYhvr5zR5u',
            GOOGLE_LOGIN_KEY: '130603976637-58mspcsh0atqk1l30kip3o1t12mmp6ac.apps.googleusercontent.com',
            GOOGLE_ANALYTICS_KEY: 'UA-20319875-9',
            MIXPANEL_WEB_KEY: 'd51db9fe1de7a6d50989072372ff6fd5',
            MIXPANEL_INSTANTS_KEY: 'cc5a187cf67d7468c92c369d0bd340bb',
            MIXPANEL_STORIES_KEY: '7e89ee5c33b454c791c714b7a0b7e56f'
        };
}

export default env;
