let config;

switch (process.env.NODE_ENV) {
    case 'production':
        config = {
            apiUrl: 'https://ssc-api-prod.scrollmotion.com',
            clientId: 'U13ewq4rVCkZwO8CE7Ni0QnumUakPNs1mY28BBvC',
            stripeKey: 'pk_live_YFQ8f79SvSww5RwZUkz6xoXo',
            googleKey: '636931751579-f7m3fhqrc065ulokn0a92okl9r8j1q1d.apps.googleusercontent.com'
        };
        break;
    case 'staging':
        config = {
            apiUrl: 'https://ssc-api-qa.scrollmotion.com',
            clientId: '1tmA4Xme0NnEws5WQ5m64fGUx8uMOYyCpUpAEAn2',
            stripeKey: 'pk_test_GlKgm5u2E280cFtYhvr5zR5u',
            googleKey: '636931751579-f7m3fhqrc065ulokn0a92okl9r8j1q1d.apps.googleusercontent.com'
        };
        break;
    default:
        config = {
            apiUrl: 'https://ssc-api-dev.scrollmotion.com',
            clientId: 'obOaSNh6zGE70pJfRGdJHMck5MYL5JocsXLloRnR',
            stripeKey: 'pk_test_GlKgm5u2E280cFtYhvr5zR5u',
            googleKey: '130603976637-58mspcsh0atqk1l30kip3o1t12mmp6ac.apps.googleusercontent.com'
        };
}

export default config;
