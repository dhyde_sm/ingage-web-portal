import path from 'path';
import express from 'express';
import env from './config/env';

const app = express();

app.set('view engine', 'pug');

const client = path.resolve('./client');
const assets = path.resolve('./assets');

app.use('/client', express.static(client));
app.use('/assets', express.static(assets));

app.get('/', (req, res) => {
    res.redirect('/ingage/');
});

app.get(['/ingage', '/ingage/*'], (req, res) => {
    res.render('ingage', { env });
});

app.get(['/realestate', '/realestate/*'], (req, res) => {
    res.render('realestate', { env });
});

app.get('/unsubscribe', (req, res) => {
    res.render('unsubscribe', { env });
});

app.get('/resubscribed', (req, res) => {
    res.render('resubscribed', { env });
});

app.use((req, res, next) => {
    res.status(404).render('404');
});

app.listen(process.env.PORT || 80);
